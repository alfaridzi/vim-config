execute pathogen#infect()
set noswapfile
set number
set path+=**
set wildmenu
filetype plugin indent on
syntax on
set background=dark
set t_Co=256
colorscheme atlantic-dark 
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
